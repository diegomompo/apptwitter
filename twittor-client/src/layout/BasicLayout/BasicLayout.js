import React from "react";
import {Container, Row, Col} from "react-bootstrap";
import LeftMenu from "../../components/LeftMenu";

import "./BasicLayout.scss"

export default function BasicLayout(props){
    const {className, children} = props
    console.log(props)

    const ColContentBasicLayout = <Col xs={9} className="basic-layout__content">{children}</Col>
    const ColMenuBasicLayout = <Col xs={3} className="basic-layout__menu"><LeftMenu /></Col>

    const RowBasicLayout = <Row>{ColMenuBasicLayout} {ColContentBasicLayout}</Row>
    const ContainerBasicLayout = <Container className={`basic-layout ${className}`}>{RowBasicLayout}</Container>

    return (
        ContainerBasicLayout
    )
}