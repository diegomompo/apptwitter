import React , {useState} from "react";
import {Container, Row, Col, Button} from "react-bootstrap"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch, faUsers, faComment} from "@fortawesome/free-solid-svg-icons"
import BasicModal from "../../components/Modal/BasicModal";
import SignUpForm from "../../components/SignUpForm";
import LogoWhiteTwittor from "../../assets/png/logo-white.png"
import LogoGreenTwittor from "../../assets/png/logo.png"
import SignInForm from "../../components/SingInForm";

import "./SignInSingUp.scss"

export default function SignInSingUp(props) {

    const {setRefreshCheckLogin} = props
    const [showModal, setShowModal] = useState(false);
    const [contentModal, setContentModal] = useState(null);

    const openModal = content => {
        setShowModal(true)
        setContentModal(content)
    }

    const RightComponentSinSup = <RightComponent
        openModal={openModal}
        setShowModal={setShowModal}
        setRefreshCheckLogin={setRefreshCheckLogin}
    />
    const LeftComponentSinSup = <LeftComponent />
    const RowSinSup = <Row>{LeftComponentSinSup}{RightComponentSinSup}</Row>
    const ContainerSinSup  = <Container className="sigin-singup" fluid>{RowSinSup}</Container>
    const BasicModalSinSUp = <BasicModal show={showModal} setShow={setShowModal}>{contentModal}</BasicModal>
    return (
        <>
            {ContainerSinSup}
            {BasicModalSinSUp}
        </>
    )
}
function LeftComponent() {

    let imgTwittorGreen, propsTwittorGreen
    let divLeft, divLeftChild

    let h2T1Left, T1Left, h2T1LeftChild
    let h2T2Left, T2Left, h2T2LeftChild
    let h2T3Left, T3Left, h2T3LeftChild

    T1Left = "  Sigue lo que le interesa"
    T2Left = "  Entérate de qué esta hablando la gente"
    T3Left = "  Unéte a la conversación"

    const searchComments = <FontAwesomeIcon icon={faComment} />
    const searchUsers = <FontAwesomeIcon icon={faUsers} />
    const searchIcon = <FontAwesomeIcon icon={faSearch} />

    h2T1LeftChild= [searchIcon, T1Left]
    h2T2LeftChild= [searchUsers, T2Left]
    h2T3LeftChild= [searchComments, T3Left]

    h2T1Left = React.createElement('h2',  null, h2T1LeftChild)
    h2T2Left = React.createElement('h2', null, h2T2LeftChild)
    h2T3Left = React.createElement('h2', null, h2T3LeftChild)

    divLeftChild = [h2T1Left, h2T2Left, h2T3Left]
    divLeft = React.createElement('div', null , divLeftChild)

    propsTwittorGreen = {src : LogoGreenTwittor , alt: "Twittor"}
    imgTwittorGreen = React.createElement('img', propsTwittorGreen, null)

    const ColLeft = <Col className = "sigin-singup__left" xs={6}>{imgTwittorGreen}{divLeft}</Col>

    return(
        ColLeft
    )

}
function RightComponent(props) {

    const {openModal, setShowModal, setRefreshCheckLogin} = props

    let divRight, divRightChild
    let imgTwittorWhite, propsTwittorWhite
    let h2T1Right, T1Right
    let h3T2Right, T2Right


    const ButtonRegistrerRight = <Button variant={"primary"} onClick={() => openModal(<SignUpForm setShowModal={setShowModal} />)}>Regístrate</Button>
    const ButtonLoginRight= <Button
                            variant={"outline-primary"}
                            onClick={() => openModal(<SignInForm setRefreshCheckLogin={setRefreshCheckLogin}/>)}>
                            Iniciar Sesión
                            </Button>

    T2Right = "Únete a Twittor hoy mismo"
    T1Right = "Mira lo que está pasando en el mundo en este momento"

    h3T2Right = React.createElement('h3', null, T2Right)
    h2T1Right = React.createElement('h2', null, T1Right)

    propsTwittorWhite= {src : LogoWhiteTwittor , alt: "Twittor"}
    imgTwittorWhite= React.createElement('img', propsTwittorWhite, null)

    divRightChild = [imgTwittorWhite, h2T1Right , h3T2Right, ButtonRegistrerRight, ButtonLoginRight]
    divRight = React.createElement('div', null , divRightChild)

    const colRight = <Col className="sigin-singup__right">{divRight}</Col>

    return(
       colRight
    )

}