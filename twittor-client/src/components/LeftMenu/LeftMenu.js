import React from "react";

import BasicLayout from "../../layout/BasicLayout";
import {Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHome, faUser, faUsers, faPowerOff} from "@fortawesome/free-solid-svg-icons";
import LogoWhite from "../../assets/png/logo-white.png"

import "./LeftMenu.scss"

export default function LeftMenu() {

    let divLeftMenu, propsDivLeftMenu, childDivLeftMenu
    let imgLeftMenu, propsImgLeftMenu

    const ButtonTwittoarLeftMenu = <Button>Twittoar</Button>

    const IconoCerrarLeftMenu = <FontAwesomeIcon icon={faPowerOff} />
    const LinkCerrarLeftMenu = <Link to="/logout">{IconoCerrarLeftMenu} Cerrar Sesión</Link>

    const IconoPerfilLeftMenu = <FontAwesomeIcon icon={faUser} />
    const LinkPerfilLeftMenu = <Link to="/user">{IconoPerfilLeftMenu} Perfil</Link>

    const IconoUsuariosLeftMenu = <FontAwesomeIcon icon={faUsers} />
    const LinkUsuariosLeftMenu = <Link to="/users">{IconoUsuariosLeftMenu} Usuarios</Link>

    const IconoInicioLeftMenu = <FontAwesomeIcon icon={faHome} />
    const LinkInicioLeftMenu = <Link to="/">{IconoInicioLeftMenu} Inicio</Link>

    propsImgLeftMenu={className: "logo" , src: LogoWhite, alt : "Twittor"}
    imgLeftMenu = React.createElement("img", propsImgLeftMenu)

    propsDivLeftMenu = {className : "left-menu"}
    childDivLeftMenu = [imgLeftMenu, LinkInicioLeftMenu, LinkUsuariosLeftMenu,
                        LinkPerfilLeftMenu, LinkCerrarLeftMenu, ButtonTwittoarLeftMenu]
    divLeftMenu = React.createElement("div", propsDivLeftMenu, childDivLeftMenu)

    return (
        divLeftMenu
    )
}