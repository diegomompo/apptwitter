import React, {useState} from "react";
import {Form, Button, Spinner} from "react-bootstrap"
import {values, size} from "lodash"
import {toast} from "react-toastify";

import {isEmailValid} from "../../utils/validations"

import "./SingInForm.scss"
import {signInApi, setTokenApi} from "../../api/auth";


export default function SignInForm(props) {
    const {setRefreshCheckLogin} = props
    console.log(props)
    const [formData, setFormData] = useState(initalFormValue());
    const [signInLoading, setSignInLoading] = useState(false);

    const onSubmit = e => {
        e.preventDefault()

        let validCount = 0
        values(formData).some(value => {
            value && validCount++
            return null
        })

        if(size(formData)  !== validCount){
            toast.warning("Completa todos los campos del formulario")
        }else{
            if (!isEmailValid(formData.email)) {
                toast.warning("Email inválido")
            }else{
                setSignInLoading((true))
                signInApi(formData).then(response => {
                    if (response.message){
                        toast.warning(response.message)
                    }else{
                        setTokenApi(response.token)
                        setRefreshCheckLogin(true)
                    }
                }).catch(() => {
                    toast.error("Error del servidor, inténtelo más tarde")
                })
                    .finally(() => {
                        setSignInLoading(false)
                    })
            }
        }

        console.log(validCount)
    }

    const onChange = e => {
        console.log(e.target.name)
        setFormData({...formData, [e.target.name] : e.target.value})
    }

    let divSinForm, propsDivSinForm, divSinFormChild
    let h2T1SinForm, T1SinForm

    const ButtonSinForm = <Button variant={"primary"} type={"submit"}>
        {!signInLoading ? "Iniciar Sesión": <Spinner animation="border"/>}
        </Button>

    const FormCrtPassSinForm2 = <Form.Control type="password" placeholder="Contraseña" name= "password" defaultValue={formData.password}></Form.Control>
    const FormGroupSinForm2 = <Form.Group className={"form-group"}>{FormCrtPassSinForm2} <br/></Form.Group>

    const FormCrtEmailSinForm1 = <Form.Control type="email" placeholder="Correo electrónico" name="email" defaultValue={formData.email}></Form.Control>
    const FormGroupSinForm1 = <Form.Group className={"form-group"}>{FormCrtEmailSinForm1} <br/></Form.Group>

    let FormSinFormChild = [FormGroupSinForm1, FormGroupSinForm2, ButtonSinForm]
    const FormSinForm = <Form onSubmit={onSubmit}  onChange={onChange}>{FormSinFormChild}</Form>

    T1SinForm = "Entrar"
    h2T1SinForm = React.createElement("h2", null, T1SinForm)

    divSinFormChild = [h2T1SinForm, FormSinForm]
    propsDivSinForm = {className : "sign-up-form"}
    divSinForm = React.createElement("div", propsDivSinForm, divSinFormChild)

    return (
        divSinForm
    )
}
function initalFormValue() {
    return {
        email: "",
        password: ""
    }

}