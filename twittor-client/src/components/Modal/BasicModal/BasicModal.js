import React from "react";
import {Modal} from "react-bootstrap";
import LogoWhiteTwittor from "../../../assets/png/logo-white.png"

import "./BasicModal.scss"

export default function BasicModal(props) {

    const { show, setShow, children }= props

    let propsImgTWhiteBM = {src : LogoWhiteTwittor , alt: "Twittor"}
    let imgTwittorWhiteBModal = React.createElement("img", propsImgTWhiteBM, null )

    const ModalTitleBasicModal = <Modal.Title>{imgTwittorWhiteBModal}</Modal.Title>
    const ModalHeaderBasicModal = <Modal.Header>{ModalTitleBasicModal}</Modal.Header>

    const ModalBodyBasicModal = <Modal.Body>{children}</Modal.Body>
    const ModalBasicModal =
        <Modal className="basic-modal" show={show} onHide={() => setShow(false)} centered size="lg">
            {ModalHeaderBasicModal}{ModalBodyBasicModal}
        </Modal>

    return(

        ModalBasicModal
    );
};
