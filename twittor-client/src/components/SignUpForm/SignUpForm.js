import React, {useState} from "react";
import {Row, Col, Form, Button, Spinner} from "react-bootstrap"
import {values, size} from "lodash"
import {toast} from "react-toastify";
import {isEmailValid} from "../../utils/validations"
import {SignUpApi} from "../../api/auth";

import "./SignUpForm.scss"


export default function SignUpForm(props) {
    const {setShowModal} = props;
    const [formData, setFormData] = useState(initialFormValue());
    const [signUpLoading, setSignUpLoading] = useState(false);


    const onSubmit = e => {
       e.preventDefault()
        console.log(formData)

        let validCount = 0

        values(formData).some(value => {
            value && validCount++
            return null
        })

        if(validCount !== size(formData)){
            toast.warning("Completa todos los campos del formulario")
        }else {
            if (!isEmailValid(formData.email)) {
                toast.warning("Email inválido")
            } else if (formData.password !== formData.repeatPassword){
                toast.warning("Las contraseñas tienen que ser iguales")
            } else if (size(formData.password) < 6){
                toast.warning("La contraseña tiene que tener al menos 6 caracteres")
            }else{
                setSignUpLoading(true)
                SignUpApi(formData).then(response => {
                    if(response.code){
                        toast.warning(response.message)
                    }else{
                        toast.success("El registro ha sido correcto")
                        setShowModal(false)
                        setFormData(initialFormValue)
                    }
                }).catch(() => {
                    toast.error("Error del servidor. Inténtelo más tarde")
                }).finally(() => {
                    setSignUpLoading(false)
                })
            }
        }

   }

   const onChange = e => {
       console.log(e.target.name)
        setFormData({...formData, [e.target.name] : e.target.value})
   }
   let divSupForm, propsDivSupForm, divSupFormChild
    let h2T1SupForm, T1SupForm


    const ButtonSupForm = <Button variant={"primary"} type={"submit"}>
        {!signUpLoading ? "Registrarse": <Spinner animation="border"/>}
    </Button>

    const FormCrtRepPassSupForm3 = <Form.Control type="password" placeholder="Repetir Contraseña" name="repeatPassword"></Form.Control>
    const ColRepPassSupForm3 = <Col>{FormCrtRepPassSupForm3}</Col>
    const FormCrtPassSupForm3 = <Form.Control type="password" placeholder="Contraseña" name="password" defaultValue={formData.password}></Form.Control>
    const ColPassSupForm3 = <Col>{FormCrtPassSupForm3}</Col>
    const RowSupForm3 = <Row>{ColPassSupForm3} {ColRepPassSupForm3}</Row>
    const FormGroupSupForm3 = <Form.Group className={"form-group"}>{RowSupForm3}</Form.Group>

    const FormCrtEmailSupForm2 = <Form.Control type="email" placeholder="Correo electrónico" name="email" defaultValue={formData.email}></Form.Control>
    const FormGroupSupForm2 = <Form.Group className={"form-group"}>{FormCrtEmailSupForm2} <br/></Form.Group>


    const FormCrtApSupForm1 = <Form.Control type="text" placeholder="Apellidos" name="apellidos" defaultValue={formData.apellidos} ></Form.Control>
    const ColApellidosSupForm1 = <Col>{FormCrtApSupForm1}</Col>

    const FormCrtNomSupForm1 = <Form.Control type="text" name="nombre" placeholder="Nombre" defaultValue={formData.nombre}></Form.Control>
    const ColNombreSupForm1 = <Col>{FormCrtNomSupForm1}</Col>

    const RowSupForm1 = <Row>{ColNombreSupForm1} {ColApellidosSupForm1}</Row>
    const FormGroupSupForm1 = <Form.Group className={"form-group"}>{RowSupForm1} <br/></Form.Group>


    let FormSupFormChild = [FormGroupSupForm1, FormGroupSupForm2, FormGroupSupForm3 , ButtonSupForm]
   const FormSupForm = <Form onSubmit={onSubmit} onChange={onChange}>{FormSupFormChild}</Form>

   T1SupForm = "Crea tu Cuenta"
   h2T1SupForm = React.createElement("h2", null, T1SupForm)

   divSupFormChild = [h2T1SupForm, FormSupForm]
   propsDivSupForm = {className : "sign-up-form"}
   divSupForm = React.createElement("div", propsDivSupForm, divSupFormChild)

    return(
        divSupForm
    )
}

function initialFormValue() {
    return {
        nombre: "",
        apellidos: "",
        email: "",
        password: "",
        repeatPassword: ""
    }
}