package middlew

import (
	"gitlab.com/diegomompo/apptwitter/bd"
	"net/http"
)
//FUNCIÓN PARA CHECKEAR EL MIDDLEW QUE PUEDE CONOCER EL ESTADO DE LA BBDD
func CheckConnection(next http.HandlerFunc) http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request){
		if bd.CheckConnection() == 0{
			http.Error(w, "Conexión perdida con la base de datos", 500)
		}
		next.ServeHTTP(w, r)
	}
}
