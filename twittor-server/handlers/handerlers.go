package handlers

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/diegomompo/apptwitter/middlew"
	"gitlab.com/diegomompo/apptwitter/routers"
)
//Funcion puerto, handler y escucha el servidor
func Manejadores(){
	router := mux.NewRouter()

	router.HandleFunc("/registro", middlew.CheckConnection(routers.Registro)).Methods("POST")
	router.HandleFunc("/login", middlew.CheckConnection(routers.Login)).Methods("POST")
	router.HandleFunc("/verperfil", middlew.CheckConnection(middlew.ValidoJWT(routers.VerPerfil))).Methods("GET")
	router.HandleFunc("/modificarPerfil", middlew.CheckConnection(middlew.ValidoJWT(routers.ModificarPerfil))).Methods("PUT")

	router.HandleFunc("/tweet", middlew.CheckConnection(middlew.ValidoJWT(routers.GraboTweet))).Methods("POST")
	router.HandleFunc("/leoTweets", middlew.CheckConnection(middlew.ValidoJWT(routers.LeoTweet))).Methods("GET")
	router.HandleFunc("/eliminarTweet", middlew.CheckConnection(middlew.ValidoJWT(routers.EliminarTweet))).Methods("DELETE")

	router.HandleFunc("/subirAvatar", middlew.CheckConnection(middlew.ValidoJWT(routers.SubirAvatar))).Methods("POST")
	router.HandleFunc("/subirBanner", middlew.CheckConnection(middlew.ValidoJWT(routers.SubirBanner))).Methods("POST")
	router.HandleFunc("/obtenerAvatar", middlew.CheckConnection(routers.ObtenerAvatar)).Methods("GET")
	router.HandleFunc("/obtenerBanner", middlew.CheckConnection(routers.ObtenerBanner)).Methods("GET")

	router.HandleFunc("/altaRelacion", middlew.CheckConnection(middlew.ValidoJWT(routers.AltaRelacion))).Methods("POST")
	router.HandleFunc("/bajaRelacion", middlew.CheckConnection(middlew.ValidoJWT(routers.BajaRelacion))).Methods("DELETE")
	router.HandleFunc("/consultaRelacion", middlew.CheckConnection(middlew.ValidoJWT(routers.ConsultaRelacion))).Methods("GET")

	router.HandleFunc("/listaUsuarios", middlew.CheckConnection(middlew.ValidoJWT(routers.ListaUsuarios))).Methods("GET")
	router.HandleFunc("/leoTweetsSeguidores", middlew.CheckConnection(middlew.ValidoJWT(routers.LeoTweetsSeguidores))).Methods("GET")

	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "8080"
	}
	handler := cors.AllowAll().Handler(router)
	log.Fatal(http.ListenAndServe(":" + PORT, handler))
}