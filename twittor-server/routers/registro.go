package routers

import (
	"encoding/json"
	"net/http"
	"gitlab.com/diegomompo/apptwitter/bd"
	"gitlab.com/diegomompo/apptwitter/models"
)
//FUNCION PARA CREAR EL REGISTRO DE USUARIO EN BBDD

func Registro(w http.ResponseWriter, r *http.Request){

	var t models.Usuario
	err:= json.NewDecoder(r.Body).Decode(&t)

	if err != nil{
		http.Error(w, "Error en los datos recibidos" + err.Error(), 400)
		return
	}

	if len(t.Email)==0{
		http.Error(w, "El email del usuario es obligatorio" + err.Error(), 400)
		return
	}
	if len(t.Password)<6{
		http.Error(w, "El password de usuario debe tener una contraseña de 6 o más caracteres" + err.Error(), 400)
		return
	}

	_,encontrado,_ := bd.CheckYaExisteUsuario(t.Email)

	if encontrado == true{
		http.Error(w, "El email introducido ya existe" + err.Error(), 400)
		return
	}

	_, status, err := bd.InsertoRegistro(t)

	if err != nil{
		http.Error(w, "Se produjo un error al intentar registrar el usuario" + err.Error(), 400)
		return
	}

	if status == false{
		http.Error(w, "No se ha podido registrar el usuario", 400)
		return
	}
	w.WriteHeader(http.StatusCreated)

}
