package routers

import (
	"encoding/json"
	"net/http"
	"gitlab.com/diegomompo/apptwitter/bd"
	"gitlab.com/diegomompo/apptwitter/models"
)

//FUNCION QUE MODIFICA EL PERFIL
func ModificarPerfil(w http.ResponseWriter, r *http.Request){

	var t models.Usuario

	err := json.NewDecoder(r.Body).Decode(&t)

	if err != nil{
		http.Error(w, "Datos incorrectos" + err.Error(), 400)
		return
	}
	var status bool
	status, err =  bd.ModificoRegistro(t, IDUsuario)

	if err != nil {
		http.Error(w, "Se produjo un error al intentar modificar el perfil. Inténtalo otra vez" + err.Error(), 400)
		return
	}
	if status != false{
		http.Error(w, "No se ha logrado modificar el perfil" + err.Error(), 400)
		return
	}
	w.WriteHeader(http.StatusCreated)
}
