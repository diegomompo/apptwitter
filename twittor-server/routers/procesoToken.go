package routers

import(
	"errors"
	"strings"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/diegomompo/apptwitter/bd"
	"gitlab.com/diegomompo/apptwitter/models"
)

//variable email de modelo
var Email string

//variable usuario de modelo
var IDUsuario string

//FUNCION PARA EXTRAER VALORES DEL TOKEN
func ProcesoToken(tk string) (*models.Claim, bool, string, error){
	miClave := []byte("MastersdelDesarrollo_grupodeFacebook")
	claims := &models.Claim{}

	splitToken := strings.Split(tk, "Bearer")
	if len(splitToken) != 2{
		return claims, false, string(""), errors.New("formato de token inválido")
	}

	tk = strings.TrimSpace(splitToken[1])

	tkn, err := jwt.ParseWithClaims(tk, claims, func(token *jwt.Token) (interface{}, error) {
		return miClave, nil
	})
	if err == nil {
		_, encontrado, _ := bd.CheckYaExisteUsuario(claims.Email)
		if encontrado == true {
			Email = claims.Email
			IDUsuario = claims.ID.Hex()
		}
		return claims, encontrado, IDUsuario, nil
	}
	if !tkn.Valid{
		return claims, false, string(""), errors.New("Token Inválido")
	}
	return claims, false, string(""), err
}
