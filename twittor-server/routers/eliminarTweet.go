package routers

import (
	"net/http"
	"gitlab.com/diegomompo/apptwitter/bd"
)

//FUNCION QUE ELIMINA UN TWEET
func EliminarTweet(w http.ResponseWriter, r *http.Request){
	ID:=r.URL.Query().Get("id")
	if len(ID)<1{
		http.Error(w, "Debe enviar el parámetro ID", http.StatusBadRequest)
	}

	err := bd.BorroTweet(ID, IDUsuario)
	if err != nil{
		http.Error(w, "Se produjo un error al intentar eliminar el tweet" + err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Conent-type", "application/json")
	w.WriteHeader(http.StatusCreated)
}
