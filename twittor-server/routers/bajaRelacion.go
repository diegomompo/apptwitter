package routers

import (
	"gitlab.com/diegomompo/apptwitter/bd"
	"gitlab.com/diegomompo/apptwitter/models"
	"net/http"
)

//FUNCION QUE BORRA UNA RELACION
func BajaRelacion(w http.ResponseWriter, r *http.Request) {

	ID := r.URL.Query().Get("id")
	if len(ID)<1{
		http.Error(w, "El ID es obligatorio", http.StatusBadRequest)
		return
	}

	var t models.Relacion
	t.UsuarioID = IDUsuario
	t.UsuarioRelacionID = ID

	status, err := bd.BorroRelacion(t)

	if err != nil{
		http.Error(w, "Se produjo un error al intentar la relación" + err.Error(), http.StatusBadRequest)
		return
	}
	if status == false{
		http.Error(w, "No se ha conseguido intentar la relación" + err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusCreated)
}