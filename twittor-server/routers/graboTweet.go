package routers

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/diegomompo/apptwitter/models"
	"gitlab.com/diegomompo/apptwitter/bd"

)

//FUNCION QUE GRANA EL TWEET EN LA BASE DE DATOS
func GraboTweet(w http.ResponseWriter, r *http.Request){
	var mensaje models.Tweet
	err := json.NewDecoder(r.Body).Decode(&mensaje)

	registro := models.GraboTweet{
		UserID: IDUsuario,
		Mensaje: mensaje.Mensaje,
		Fecha: time.Now(),
	}

	_, status, err := bd.InsertoTweet(registro)
	if err != nil {
		http.Error(w, "Ocurrió un error al intentar insertar el registro, reintenete nuevanente" + err.Error(), 400)
		return
	}

	if status == false{
		http.Error(w, "No se ha podido insertar el tweet" , 400)
		return
	}
	w.WriteHeader(http.StatusCreated)
}
