package routers

import (
	"encoding/json"
	"net/http"
	"gitlab.com/diegomompo/apptwitter/bd"
	"gitlab.com/diegomompo/apptwitter/models"
)

//FUNCION QUE DICE SI HAY CONSULTA
func ConsultaRelacion(w http.ResponseWriter, r *http.Request) {

	ID := r.URL.Query().Get("id")

	var t models.Relacion
	t.UsuarioID = IDUsuario
	t.UsuarioRelacionID = ID

	var resp models.RespuestaConsultaRelacion

	status, err:= bd.ConsultoRelacion(t)

	if err != nil || status == false{
		resp.Status=false
	}else {
		resp.Status=true
	}
	w.Header().Set("Conent-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)
}