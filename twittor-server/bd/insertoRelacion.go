package bd

import (
	"context"
	"time"

	"gitlab.com/diegomompo/apptwitter/models"
)

//FUNCION PARA BORRAR RELACION EN BASE DE DATOS

func InsertoRelacion (t models.Relacion) (bool, error){
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := MongoCN.Database("twitter")
	col := db.Collection("relacion")

	_, err := col.InsertOne(ctx, t)
	if err != nil {
		return false, err
	}

	return true, nil
}