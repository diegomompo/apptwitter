package bd

import "golang.org/x/crypto/bcrypt"

//FUNCION QUE ENCRIPTA LA CONTRASEÑA
func EncriptarPassword(pass string)(string, error){
	costo := 8
	bytes, err := bcrypt.GenerateFromPassword([]byte(pass), costo)
	return string(bytes), err
}
