package bd

import (
	"gitlab.com/diegomompo/apptwitter/models"
	"golang.org/x/crypto/bcrypt"
)

//FUNCION PARA CHECK DEL LOGIN
func IntentoLogin(email string, password string)(models.Usuario, bool ){
	usu, encontrado, _ := CheckYaExisteUsuario(email)
	if encontrado==false{
		return usu, false
	}

	passwordBytes := []byte(password)
	var passwordBD = []byte(usu.Password)
	err:= bcrypt.CompareHashAndPassword(passwordBD, passwordBytes)

	if err!=nil{
		return usu, false
	}
	return usu, true
}

