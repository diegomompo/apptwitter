package bd

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

// MongoCN Variable para exportar BBDD
var MongoCN= conectarBD()
var clientOptions = options.Client().ApplyURI("mongodb+srv://diegomompo:Gol%40ng2021@twittermompo.sv7sg.mongodb.net/twitter?retryWrites=true&w=majority")

//ConectarBD: Función que conecta la base de datos

func conectarBD() *mongo.Client {
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil{
		log.Fatal(err.Error())
		return client
	}
	err = client.Ping(context.TODO(), nil)
	if err != nil{
		log.Fatal(err.Error())
		return client
	}
	log.Println("Conexíon existosa con la BD")
	return client
}

//CheckConnetion: Función de ping a la BBDD

func CheckConnection () int{
	err := MongoCN.Ping(context.TODO(), nil)
	if err != nil{
		log.Fatal(err)
		return 0
	}
	return 1
}